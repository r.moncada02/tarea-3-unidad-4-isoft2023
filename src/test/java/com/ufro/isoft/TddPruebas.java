package com.ufro.isoft;

import com.ufro.isoft.models.Persona;
import com.ufro.isoft.services.PersonaService;
import org.junit.jupiter.api.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TddPruebas {
    PersonaService personaService;
    Persona persona;

    @BeforeAll
    static void all() {
        System.out.println("BEFORE ALL");
    }

    @BeforeEach
    void setup() {
        this.personaService = new PersonaService();
        System.out.println("BEFORE EACH");
    }

    @Test
    public void crearPersona(){
        Persona persona =new Persona();
        persona.setNombre("pedrito");
        persona.setApellidoPaterno("rojo");
        persona.setApellidoMaterno("morales");
        persona.setRut("16.827.524-1");
        persona.setNumero("977323817");
        persona.setEdad(15);
        Assertions.assertNotNull(persona);
    }
    @Test
    @DisplayName("Validación nombre")
    public void validarNombre() {
        Persona persona = new Persona();
        persona.setNombre("pedrito");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true, personaService.validarNombre(persona));
    }

    @Test
    @DisplayName("Validación de apellido paterno")
    public void validarApellidoPaterno() {
        Persona persona = new Persona();
        persona.setApellidoPaterno("rojo");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true, personaService.validarApellidoPaterno(persona));
    }

    @Test
    @DisplayName("Validación de apellido materno")
    public void validarApellidoMaterno() {
        Persona persona = new Persona();
        persona.setApellidoMaterno("morales");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true, personaService.validarApellidoMaterno(persona));
    }

    @Test
    @DisplayName("Validación de rut")
    public void validarRut() {
        Persona persona = new Persona();
        persona.setRut("16.827.524-1");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true, personaService.validarRut(persona));
    }

    @Test
    @DisplayName("Validación de numero")
    public void validarNumero() {
        Persona persona = new Persona();
        persona.setNumero("977323817");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true, personaService.validarNumero(persona));
    }

    @Test
    @DisplayName("Validar largo de numero")
    public void validarLargoNumero(){
        Persona persona = new Persona();
        persona.setNumero("977323817");
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true,personaService.validarLargoNumero(persona));
    }


    @Test
    @DisplayName("Validar edad")
    public void validarEdad(){
        Persona persona = new Persona();
        persona.setEdad(15);
        PersonaService personaService = new PersonaService();
        Assertions.assertEquals(true,personaService.validarEdad(persona));
    }




}
