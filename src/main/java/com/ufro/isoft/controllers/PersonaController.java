package com.ufro.isoft.controllers;

import com.ufro.isoft.models.Persona;
import com.ufro.isoft.services.PersonaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {

    @PostMapping("/validar")
    public ResponseEntity<String> validarPersona(@RequestBody Persona persona) {
        try {
            PersonaService personaService = new PersonaService();

            personaService.validarNombre(persona);
            personaService.validarApellidoPaterno(persona);
            personaService.validarApellidoMaterno(persona);
            personaService.validarRut(persona);
            personaService.validarNumero(persona);
            personaService.validarLargoNumero(persona);
            personaService.validarEdad(persona);

            return ResponseEntity.ok("La persona ha sido validada correctamente");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

}