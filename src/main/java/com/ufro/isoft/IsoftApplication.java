package com.ufro.isoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsoftApplication.class, args);
	}

}
