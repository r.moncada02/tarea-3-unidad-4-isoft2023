package com.ufro.isoft.services;

import com.ufro.isoft.models.Persona;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonaService {
    public boolean validarNombre(Persona persona) {
        String regex = "^[a-zA-Z]+$";
        if (persona.getNombre().matches(regex)) {
            System.out.println("Nombre válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El nombre debe contener solo texto");
        }
    }

    public boolean validarApellidoPaterno(Persona persona) {
        String regex = "^[a-zA-Z]+$";
        if (persona.getApellidoPaterno().matches(regex)) {
            System.out.println("Apellido válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El apellido debe contener solo texto");
        }
    }

    public boolean validarApellidoMaterno(Persona persona) {
        String regex = "^[a-zA-Z]+$";
        if (persona.getApellidoMaterno().matches(regex)) {
            System.out.println("Apellido válido...");
            return true;
        } else {
            throw new IllegalArgumentException("El apellido debe contener solo texto");
        }
    }

    public boolean validarRut(Persona persona) {
        String rut = persona.getRut();
        String charsToRetain = "0123456789-";
        rut = rut.replace(".", "");
        Pattern pattern = Pattern.compile("^[0-9]+-[0-9kK]{1}$");
        Matcher matcher = pattern.matcher(rut);
        if (matcher.matches() == false) throw new IllegalArgumentException("Formato incorrecto");
        System.out.println("Rut correcto");
        return true;
    }

    public boolean validarNumero(Persona persona) {
        String regex = "[0-9]+";
        if (persona.getNumero().matches(regex)) {
            System.out.println("Numero valido...");
            return true;
        } else {
            throw new IllegalArgumentException("El telefono debe contener solo numeros");
        }

    }
    public boolean validarLargoNumero(Persona persona) {
        if (persona.getNumero().length() == 9) {
            System.out.println("Largo de numero valido...");
            return true;
        } else {
            throw new IllegalArgumentException("El numero debe ser de largo 9");

        }
    }
    public boolean validarEdad(Persona persona) {
        if (persona.getEdad() >= 0) {
            System.out.println("La edad es valida...");
            return true;
        } else {
            throw new IllegalArgumentException("La edad no puede ser negativa");
        }
    }

}